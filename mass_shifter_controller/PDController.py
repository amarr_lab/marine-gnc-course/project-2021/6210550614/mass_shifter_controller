from curses import KEY_PPAGE
import numpy as np


class PDController:
    def __init__(self, k_p, k_d):
        # variable declaration
        self.k_p = k_p
        self.k_d = k_d

        self.P = 0.0
        self.D = 0.0

        self.err = 0.0
        self.prev_err = 0.0
        self.diff_err = 0.0

        self.t = 0.0
        self.prev_t = -1.0

    def calculate_pd(self, desired, actual, t):

        # calculate error

        self.err = desired - actual
        self.t = t
        dt = self.t - self.prev_t

        if(self.prev_t == -1.0):
            # the derivative error = 0 for the first time
            self.diff_err = 0.0

        elif(dt > 0.0):
            # calculate derivative error
            self.diff_err = (self.err - self.prev_err)/dt

        # calculate pd controller

        self.P = self.k_p * self.err
        self.D = self.k_d * self.diff_err

        PD = self.P + self.D

        return PD

    def reconfig_param(self, k_p, k_d):
        # update variables
        self.k_p = k_p
        self.k_d = k_d

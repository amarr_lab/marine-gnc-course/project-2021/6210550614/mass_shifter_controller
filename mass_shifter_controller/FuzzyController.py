import numpy as np
import skfuzzy
from skfuzzy import control as ctrl
import matplotlib as plt


class SelfTuningController:

    def __init__(self, err_z, err_theta, err_diff_theta):

        self.e_z = err_z
        self.e_theta = err_theta
        self.e_diff_theta = err_diff_theta

        self.gam = ctrl.Consequent(np.arange(-0.36, -0.35, 0.001), "gammar")
        self.kp = ctrl.Consequent(np.arange(-0.9, 0.2, 0.1), "kp")
        self.kd = ctrl.Consequent(np.arange(-2.0, 0.25, 0.25), "kd")

        self.err_z = ctrl.Antecedent(np.arange(-2, 2.5, 0.5), "err_z")
        self.err_theta = ctrl.Antecedent(
            np.arange(-0.6, 0.7, 0.1), "err_theta")
        self.err_diff_theta = ctrl.Antecedent(
            np.arange(-0.3, 0.35, 0.05), "err_diff_theta")

        # calling functions
        self.membership_function()
        self.create_rules()
        self.control_system()

        self.sim.input['err_z'] = self.e_z
        self.sim.input['err_theta'] = self.e_theta
        self.sim.input['err_diff_theta'] = self.e_diff_theta

        self.sim.compute()
        print("gammar: ", self.sim.output['gammar'])
        print("kp: ", self.sim.output['kp'])
        print("kd: ", self.sim.output['kd'])

    def membership_function(self):
        # membership function for input

        # depth error (fig.9)
        self.err_z['BN'] = skfuzzy.trapmf(
            self.err_z.universe, [-2, -2, -1.35, -0.6])
        self.err_z['SN'] = skfuzzy.trapmf(
            self.err_z.universe, [-1.35, -0.6, 0, 0.01])
        self.err_z['SP'] = skfuzzy.trapmf(
            self.err_z.universe, [-0.01, 0, 0.6, 1.35])
        self.err_z['BP'] = skfuzzy.trapmf(
            self.err_z.universe, [0.6, 1.35, 2, 2])

        # self.err_z.view()
        # plt.grid()

        # pitch error (fig.9)
        self.err_theta['BN'] = skfuzzy.trapmf(
            self.err_theta.universe, [-0.6, -0.6, -0.5, -0.4])
        self.err_theta['MN'] = skfuzzy.trapmf(
            self.err_theta.universe, [-0.5, -0.4, -0.20, -0.1])
        self.err_theta['SN'] = skfuzzy.trapmf(
            self.err_theta.universe, [-0.2, -0.1, 0.0, 0.0])
        self.err_theta['SP'] = skfuzzy.trapmf(
            self.err_theta.universe, [-0.1, 0.0, 0.1, 0.2])
        self.err_theta['MP'] = skfuzzy.trapmf(
            self.err_theta.universe, [0.1, 0.2, 0.4, 0.5])
        self.err_theta['BP'] = skfuzzy.trapmf(
            self.err_theta.universe, [0.4, 0.5, 0.6, 0.6])

        # self.err_theta.view()
        # plt.grid()

        # diff. pitch error (fig.9)
        self.err_diff_theta['BN'] = skfuzzy.trapmf(
            self.err_diff_theta, [-0.3, -0.3, -0.2, -0.15])
        self.err_diff_theta['MN'] = skfuzzy.trapmf(
            self.err_diff_theta, [-0.2, -0.15, -0.1, -0.05])
        self.err_diff_theta['SN'] = skfuzzy.trapmf(
            self.err_diff_theta, [-0.1, -0.05, 0, 0.01])
        self.err_diff_theta['SP'] = skfuzzy.trapmf(
            self.err_diff_theta, [-0.01, 0, 0.05, 0.1])
        self.err_diff_theta['MP'] = skfuzzy.trapmf(
            self.err_diff_theta, [0.05, 0.1, 0.15, 0.2])
        self.err_diff_theta['BP'] = skfuzzy.trapmf(
            self.err_diff_theta, [0.15, 0.2, 0.3, 0.3])

        # self.err_diff_theta.view()
        # plt.grid()

        # membership function for output

        # gain gammar
        self.gam['BN'] = skfuzzy.trimf(self.gam, [-0.36, -0.36, -0.355])
        self.gam['MN'] = skfuzzy.trimf(self.gam, [-0.36, -0.355, -0.35])
        self.gam['SN'] = skfuzzy.trimf(self.gam, [-0.355, -0.35, -0.35])

        # self.gam.view()
        # plt.grid()

        # gain kp
        self.kp['BN'] = skfuzzy.trapmf(self.kp, [-0.9, -0.9, -0.7, -0.5])
        self.kp['MN'] = skfuzzy.trapmf(self.kp, [-0.7, -0.5, -0.3, -0.1])
        self.kp['SN'] = skfuzzy.trapmf(self.kp, [-0.3, -0.1, 0.1, 0.1])

        # self.kp.view()
        # plt.grid()

        # gain kd
        self.kd['MN'] = skfuzzy.trapmf(self.kp, [-2.0, -2.0, -1.75, -1.25])
        self.kd['SN'] = skfuzzy.trapmf(self.kp, [-1.75, -1.25, -0.75, -0.25])
        self.kd['BN'] = skfuzzy.trapmf(self.kp, [-0.75, -0.25, 0, 0])

        # self.kd.view()
        # plt.grid()

    def create_rules(self):
        # create fuzzy rules for gain gammar, kp, and kd

        # gammar

        self.gam_rule_1 = ctrl.Rule(antecedent=(self.err_z['BN']),
                                    consequent=(self.gam['MN']),
                                    label='MN')
        self.gam_rule_2 = ctrl.Rule(antecedent=(self.err_z['SN'] | self.err_z['SP']),
                                    consequent=(self.gam['SN']),
                                    label='SN')
        self.gam_rule_3 = ctrl.Rule(antecedent=(self.err_z['BP']),
                                    consequent=(self.gam['BN']),
                                    label='BN')
        # kp
        self.kp_rule_1 = ctrl.Rule(antecedent=(self.err_theta['BN'] |
                                               self.err_theta['MN'] |
                                               self.err_theta['MP'] |
                                               self.err_theta['BP'] |
                                               (self.err_theta['SN'] & self.err_diff_theta['BP']) |
                                               (self.err_theta['SN'] & self.err_diff_theta['MP']) |
                                               (self.err_theta['SP'] & self.err_diff_theta['SP']) |
                                               (self.err_theta['SP'] & self.err_diff_theta['SN']) |
                                               (self.err_theta['SP'] & self.err_diff_theta['MN']) |
                                               (self.err_theta['SP'] & self.err_diff_theta['BN'])),
                                   consequent=(self.kp['BN']),
                                   label='BN')
        self.kp_rule_2 = ctrl.Rule(antecedent=((self.err_theta['SP'] & self.err_diff_theta['BP']) |
                                               (self.err_theta['SP'] & self.err_diff_theta['MP']) |
                                               (self.err_theta['SN'] & self.err_diff_theta['SP']) |
                                               (self.err_theta['SN'] & self.err_diff_theta['MN']) |
                                               (self.err_theta['BN'] & self.err_diff_theta['BN'])),
                                   consequent=(self.kp['BN']),
                                   label='SN')

        # kd
        self.kd_rule_1 = ctrl.Rule(antecedent=((self.err_theta['BN'] & self.err_diff_theta['BP']) |
                                               (self.err_theta['BN'] & self.err_diff_theta['MP']) |
                                               (self.err_theta['BN'] & self.err_diff_theta['SP']) |
                                               (self.err_theta['SN'] & self.err_diff_theta['BP']) |
                                               (self.err_theta['SP'] & self.err_diff_theta['SP']) |
                                               (self.err_theta['SP'] & self.err_diff_theta['BN']) |
                                               (self.err_theta['MP'] & self.err_diff_theta['MN']) |
                                               (self.err_theta['MP'] & self.err_diff_theta['BN']) |
                                               (self.err_theta['BP'] & self.err_diff_theta['SN']) |
                                               (self.err_theta['BP'] & self.err_diff_theta['MN']) |
                                               (self.err_theta['BP'] & self.err_diff_theta['BN'])),
                                   consequent=(self.kd['MN']),
                                   label='MN')
        self.kd_rule_2 = ctrl.Rule(antecedent=((self.err_theta['MN'] & self.err_diff_theta['BP']) |
                                               (self.err_theta['MN'] & self.err_diff_theta['MP']) |
                                               (self.err_theta['SN'] & self.err_diff_theta['MP']) |
                                               (self.err_theta['SN'] & self.err_diff_theta['SN']) |
                                               (self.err_theta['SN'] & self.err_diff_theta['MN']) |
                                               (self.err_theta['SN'] & self.err_diff_theta['BN']) |
                                               (self.err_theta['SP'] & self.err_diff_theta['BP']) |
                                               (self.err_theta['SP'] & self.err_diff_theta['MP'])),
                                   consequent=(self.kd['SN']),
                                   label='SN')
        self.kd_rule_3 = ctrl.Rule(antecedent=((self.err_theta['BN'] & self.err_diff_theta['SN']) |
                                               (self.err_theta['BN'] & self.err_diff_theta['MN']) |
                                               (self.err_theta['BN'] & self.err_diff_theta['BN']) |
                                               (self.err_theta['MN'] & self.err_diff_theta['SP']) |
                                               (self.err_theta['MN'] & self.err_diff_theta['SN']) |
                                               (self.err_theta['MN'] & self.err_diff_theta['MN']) |
                                               (self.err_theta['MN'] & self.err_diff_theta['BN']) |
                                               (self.err_theta['SN'] & self.err_diff_theta['SP']) |
                                               (self.err_theta['SP'] & self.err_diff_theta['SN']) |
                                               (self.err_theta['SP'] & self.err_diff_theta['MN']) |
                                               (self.err_theta['MP'] & self.err_diff_theta['BP']) |
                                               (self.err_theta['MP'] & self.err_diff_theta['MP']) |
                                               (self.err_theta['MP'] & self.err_diff_theta['SP']) |
                                               (self.err_theta['MP'] & self.err_diff_theta['SN']) |
                                               (self.err_theta['BP'] & self.err_diff_theta['BP']) |
                                               (self.err_theta['BP'] & self.err_diff_theta['MP']) |
                                               (self.err_theta['BP'] & self.err_diff_theta['SP'])),
                                   consequent=(self.kd['BN']),
                                   label='BN')

    def control_system(self):
        self.system = ctrl.ControlSystem(rules=[self.gam_rule_1,
                                                self.gam_rule_2,
                                                self.gam_rule_3,
                                                self.kp_rule_1,
                                                self.kp_rule_2,
                                                self.kd_rule_1,
                                                self.kd_rule_2,
                                                self.kd_rule_3])
        self.sim = ctrl.ControlSystemSimulation(self.system)

from lib2to3.pytree import Node
import numpy as np
import skfuzzy
from skfuzzy import control as ctrl
import matplotlib.pyplot as plt

gam = ctrl.Consequent(np.arange(-0.36, -0.35, 0.001), "gammar")
kp = ctrl.Consequent(np.arange(-0.9, 0.2, 0.1), "kp")
kd = ctrl.Consequent(np.arange(-2.0, 0.25, 0.25), "kd")

err_z = ctrl.Antecedent(np.arange(-2, 2.5, 0.5), "err_z")
err_theta = ctrl.Antecedent(
    np.arange(-0.6, 0.7, 0.1), "err_theta")
err_diff_theta = ctrl.Antecedent(
    np.arange(-0.3, 0.35, 0.05), "err_diff_theta")

# membership function for input

# depth error (fig.9)
err_z['BN'] = skfuzzy.trapmf(
    err_z.universe, [-2, -2, -1.35, -0.6])
err_z['SN'] = skfuzzy.trapmf(
    err_z.universe, [-1.35, -0.6, 0, 0.01])
err_z['SP'] = skfuzzy.trapmf(
    err_z.universe, [-0.01, 0, 0.6, 1.35])
err_z['BP'] = skfuzzy.trapmf(
    err_z.universe, [0.6, 1.35, 2, 2])

err_z.view()
plt.grid()

# pitch error (fig.9)
err_theta['BN'] = skfuzzy.trapmf(
    err_theta.universe, [-0.6, -0.6, -0.5, -0.4])
err_theta['MN'] = skfuzzy.trapmf(
    err_theta.universe, [-0.5, -0.4, -0.20, -0.1])
err_theta['SN'] = skfuzzy.trapmf(
    err_theta.universe, [-0.2, -0.1, 0.0, 0.0])
err_theta['SP'] = skfuzzy.trapmf(
    err_theta.universe, [-0.1, 0.0, 0.1, 0.2])
err_theta['MP'] = skfuzzy.trapmf(
    err_theta.universe, [0.1, 0.2, 0.4, 0.5])
err_theta['BP'] = skfuzzy.trapmf(
    err_theta.universe, [0.4, 0.5, 0.6, 0.6])

err_theta.view()
plt.grid()

# diff. pitch error (fig.9)
err_diff_theta['BN'] = skfuzzy.trapmf(
    err_diff_theta.universe, [-0.3, -0.3, -0.2, -0.15])
err_diff_theta['MN'] = skfuzzy.trapmf(
    err_diff_theta.universe, [-0.2, -0.15, -0.1, -0.05])
err_diff_theta['SN'] = skfuzzy.trapmf(
    err_diff_theta.universe, [-0.1, -0.05, 0, 0.01])
err_diff_theta['SP'] = skfuzzy.trapmf(
    err_diff_theta.universe, [-0.01, 0, 0.05, 0.1])
err_diff_theta['MP'] = skfuzzy.trapmf(
    err_diff_theta.universe, [0.05, 0.1, 0.15, 0.2])
err_diff_theta['BP'] = skfuzzy.trapmf(
    err_diff_theta.universe, [0.15, 0.2, 0.3, 0.3])

err_diff_theta.view()
plt.grid()

# membership function for output

# gain gammar
gam['BN'] = skfuzzy.trimf(gam.universe, [-0.36, -0.36, -0.355])
gam['MN'] = skfuzzy.trimf(gam.universe, [-0.36, -0.355, -0.35])
gam['SN'] = skfuzzy.trimf(gam.universe, [-0.355, -0.35, -0.35])

gam.view()
plt.grid()

# gain kp
kp['BN'] = skfuzzy.trapmf(kp.universe, [-0.9, -0.9, -0.7, -0.5])
kp['MN'] = skfuzzy.trapmf(kp.universe, [-0.7, -0.5, -0.3, -0.1])
kp['SN'] = skfuzzy.trapmf(kp.universe, [-0.3, -0.1, 0.1, 0.1])

kp.view()
plt.grid()

# gain kd
kd['MN'] = skfuzzy.trapmf(kd.universe, [-2.0, -2.0, -1.75, -1.25])
kd['SN'] = skfuzzy.trapmf(kd.universe, [-1.75, -1.25, -0.75, -0.25])
kd['BN'] = skfuzzy.trapmf(kd.universe, [-0.75, -0.25, 0, 0])

kd.view()
plt.grid()

plt.show()

# create fuzzy rules for gain gammar, kp, and kd

# gammar

gam_rule_1 = ctrl.Rule(antecedent=(err_z['BN']),
                       consequent=(gam['MN']),
                       label='MN_gam')
gam_rule_2 = ctrl.Rule(antecedent=(err_z['SN'] | err_z['SP']),
                       consequent=(gam['SN']),
                       label='SN_gam')
gam_rule_3 = ctrl.Rule(antecedent=(err_z['BP']),
                       consequent=(gam['BN']),
                       label='BN_gam')
# kp
kp_rule_1 = ctrl.Rule(antecedent=(err_theta['BN'] |
                                  err_theta['MN'] |
                                  err_theta['MP'] |
                                  err_theta['BP'] |
                                  (err_theta['SN'] & err_diff_theta['BP']) |
                                  (err_theta['SN'] & err_diff_theta['MP']) |
                                  (err_theta['SP'] & err_diff_theta['SP']) |
                                  (err_theta['SP'] & err_diff_theta['SN']) |
                                  (err_theta['SP'] & err_diff_theta['MN']) |
                                  (err_theta['SP'] & err_diff_theta['BN'])),
                      consequent=(kp['BN']),
                      label='BN_kp')
kp_rule_2 = ctrl.Rule(antecedent=((err_theta['SP'] & err_diff_theta['BP']) |
                                  (err_theta['SP'] & err_diff_theta['MP']) |
                                  (err_theta['SN'] & err_diff_theta['SP']) |
                                  (err_theta['SN'] & err_diff_theta['MN']) |
                                  (err_theta['BN'] & err_diff_theta['BN'])),
                      consequent=(kp['BN']),
                      label='SN_kp')

# kd
kd_rule_1 = ctrl.Rule(antecedent=((err_theta['BN'] & err_diff_theta['BP']) |
                                  (err_theta['BN'] & err_diff_theta['MP']) |
                                  (err_theta['BN'] & err_diff_theta['SP']) |
                                  (err_theta['SN'] & err_diff_theta['BP']) |
                                  (err_theta['SP'] & err_diff_theta['SP']) |
                                  (err_theta['SP'] & err_diff_theta['BN']) |
                                  (err_theta['MP'] & err_diff_theta['MN']) |
                                  (err_theta['MP'] & err_diff_theta['BN']) |
                                  (err_theta['BP'] & err_diff_theta['SN']) |
                                  (err_theta['BP'] & err_diff_theta['MN']) |
                                  (err_theta['BP'] & err_diff_theta['BN'])),
                      consequent=(kd['MN']),
                      label='MN_kp')
kd_rule_2 = ctrl.Rule(antecedent=((err_theta['MN'] & err_diff_theta['BP']) |
                                  (err_theta['MN'] & err_diff_theta['MP']) |
                                  (err_theta['SN'] & err_diff_theta['MP']) |
                                  (err_theta['SN'] & err_diff_theta['SN']) |
                                  (err_theta['SN'] & err_diff_theta['MN']) |
                                  (err_theta['SN'] & err_diff_theta['BN']) |
                                  (err_theta['SP'] & err_diff_theta['BP']) |
                                  (err_theta['SP'] & err_diff_theta['MP'])),
                      consequent=(kd['SN']),
                      label='SN_kd')
kd_rule_3 = ctrl.Rule(antecedent=((err_theta['BN'] & err_diff_theta['SN']) |
                                  (err_theta['BN'] & err_diff_theta['MN']) |
                                  (err_theta['BN'] & err_diff_theta['BN']) |
                                  (err_theta['MN'] & err_diff_theta['SP']) |
                                  (err_theta['MN'] & err_diff_theta['SN']) |
                                  (err_theta['MN'] & err_diff_theta['MN']) |
                                  (err_theta['MN'] & err_diff_theta['BN']) |
                                  (err_theta['SN'] & err_diff_theta['SP']) |
                                  (err_theta['SP'] & err_diff_theta['SN']) |
                                  (err_theta['SP'] & err_diff_theta['MN']) |
                                  (err_theta['MP'] & err_diff_theta['BP']) |
                                  (err_theta['MP'] & err_diff_theta['MP']) |
                                  (err_theta['MP'] & err_diff_theta['SP']) |
                                  (err_theta['MP'] & err_diff_theta['SN']) |
                                  (err_theta['BP'] & err_diff_theta['BP']) |
                                  (err_theta['BP'] & err_diff_theta['MP']) |
                                  (err_theta['BP'] & err_diff_theta['SP'])),
                      consequent=(kd['BN']),
                      label='BN_kd')
system = ctrl.ControlSystem(rules=[gam_rule_1,
                                   gam_rule_2,
                                   gam_rule_3,
                                   kp_rule_1,
                                   kp_rule_2,
                                   kd_rule_1,
                                   kd_rule_2,
                                   kd_rule_3])
controller = ctrl.ControlSystemSimulation(system)

controller.input['err_z'] = 1.0
controller.input['err_theta'] = 0.3
controller.input['err_diff_theta'] = 0.01

controller.compute()
print("gammar: ", controller.output['gammar'])
print("kp: ", controller.output['kp'])
print("kd: ", controller.output['kd'])

# gam.view(sim=controller)
# kp.view(sim=controller)
